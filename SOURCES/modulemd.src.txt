document: modulemd
version: 2
data:
  name: pmdk
  stream: 1_fileformat_v6
  summary: Persistent Memory Development Kit (former NVML)
  description: The Persistent Memory Development Kit is a collection
               of libraries for using memory-mapped persistence,
               optimized specifically for persistent memory.
  license:
    module: [BSD]
  dependencies:
    - buildrequires:
        platform: [el8]
      requires:
        platform: [el8]
  references:
    community: http://pmem.io/pmdk

  components:
    rpms:
      pmdk:
        rationale: main pmdk library
        ref: stream-rhel-8.8.0
        buildorder: 0
      libpmemobj-cpp:
        rationale: depends on (recent version of) pmdk
        ref: stream-rhel-8.8.0
        buildorder: 10
